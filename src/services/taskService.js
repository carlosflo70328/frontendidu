import { axiosInstance } from "../helpers/axios-config";

const getUsers = () => {
  return axiosInstance.get("", {
    headers: {
      "Content-type": "application/json",
    },
  });
};

const getTaskPorId = (taskId) => {
  return axiosInstance.get(`/${taskId}`, {
    headers: {
      "Content-type": "application/json",
    },
  });
};


const crearTask = (data) => {
  return axiosInstance.post("", data, {
    headers: {
      "Content-type": "application/json",
    },
  });
};

const deleteTask = (taskId, data) => {
  return axiosInstance.delete(`/${taskId}`, {
    headers: {
      "Content-type": "application/json",
    },
  });
};

const editTask = (taskId, data) => {
  return axiosInstance.put(`/${taskId}`, data, {
    headers: {
      "Content-type": "application/json",
    },
  });
};






export {
  getUsers,
  crearTask,
  deleteTask,
  editTask,
  getTaskPorId

};
