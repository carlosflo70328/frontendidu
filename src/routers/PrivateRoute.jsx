import { Navigate } from "react-router-dom";
import Cookies from "universal-cookie";

const cookies = new Cookies();

export const PrivateRoute = ({ children }) => {
  return cookies.get("email") ? children : <Navigate to="/login" />;
};
