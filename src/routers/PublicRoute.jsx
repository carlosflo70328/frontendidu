import { Navigate } from "react-router-dom";
import Cookies from "universal-cookie";

const cookies = new Cookies();

export const PublicRoute = ({ children }) => {
  return !cookies.get("email") ? children : <Navigate to="/home" />;
};
