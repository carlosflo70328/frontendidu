import { Routes, Route, Navigate } from "react-router-dom";
import { NavbarLeft } from "../components/ui/NavbarLeft";
import { EstadoView } from "../components/estados/EstadoView";
import { ViewTask } from "../components/task/ViewTask";
import { MarcaView } from "../components/marcas/MarcaView";
import { UpdateTask } from "../components/task/UpdateTask";


export const DashboardRoutes = () => {
  return (
    <div className="d-flex flex-nowrap mainVar">
      <NavbarLeft />
      <Routes>
        <Route path="/home" element={<ViewTask />} />
        <Route path="/otrasFunciones" element={<MarcaView />} />
        <Route path="/configuraciones" element={<EstadoView />} />
        <Route
          path="/edit/:taskId"
          element={<UpdateTask />}
        />
     
        <Route path="/" element={<Navigate to="/home" replace={true} />} />
      </Routes>
    </div>
  );
};
