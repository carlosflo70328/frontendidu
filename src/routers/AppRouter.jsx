import { Route, Routes, BrowserRouter } from "react-router-dom";
// import { PrivateRoute } from "./PrivateRoute";
import { PublicRoute } from "./PublicRoute";
// import { LoginScreen } from "../components/ui/LoginScreen";
import { DashboardRoutes } from "./DashboardRoutes";

export const AppRouter = () => {
  return (
    <BrowserRouter>
      <Routes>
        {/* <Route
          path="login/*"
          element={
            <PrivateRoute>
              <Routes>
                <Route path="/*" element={<LoginScreen />} />
              </Routes>
            </PrivateRoute>
          }
        /> */}
        <Route
          path="/*"
          element={
            <PublicRoute>
              <DashboardRoutes />
            </PublicRoute>
          }
        />
      </Routes>
    </BrowserRouter>
  );
};
