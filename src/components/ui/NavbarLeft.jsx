import React from "react";
import { NavLink } from "react-router-dom";
import "./navbar.css";
import Cookies from "universal-cookie";

const cookies = new Cookies();

export const NavbarLeft = () => {
  return (
    <div
      className="d-flex flex-column flex-shrink-0 p-3 colorFondo"
      style={{ width: 280 + "px" }}
    >
      <a
        href="/home"
        className="d-flex align-items-center mb-3 mb-md-0 me-md-auto text-white text-decoration-none"
      >
        <svg className="bi pe-none me-2" width="40" height="32"></svg>
        <span className="fs-4">Prueba IDU</span>
      </a>
      <hr></hr>
      <ul className="nav nav-pills flex-column mb-auto">
        <li className="nav-item">
          <NavLink className="nav-link " activeclassname="active" to="/home">
            <i className="bi bi-house"></i> Usuarios y tareas
          </NavLink>
        </li>
       
        <li className="nav-item">
          <NavLink className="nav-link" activeclassname="active" to="/otrasFunciones">
            <i className="bi bi-badge-tm"></i> Otas funcionalidades
          </NavLink>
        </li>
        <li className="nav-item">
          <NavLink className="nav-link" activeclassname="active" to="/configuraciones">
            <i className="bi bi-device-ssd-fill"></i> Configuraciones
          </NavLink>
        </li>

      </ul>
      <hr></hr>
      <div className="dropdown">
        <div
          className="d-flex align-items-center text-white text-decoration-none dropdown-toggle"
          data-bs-toggle="dropdown"
          aria-expanded="false"
        >
          <img
            src="https://github.com/mdo.png"
            alt=""
            width="32"
            height="32"
            className="rounded-circle me-2"
          ></img>
          <strong> {cookies.get("nombre")}</strong>
        </div>
        <ul className="dropdown-menu dropdown-menu-dark text-small shadow">
          
          <li>
            <i
              className="dropdown-item"
              onClick={() => {
                cookies.remove("email", { path: "/" });
                cookies.remove("estado", { path: "/" });
                cookies.remove("nombre", { path: "/" });
                cookies.remove("rol", { path: "/" });

                window.location.href = "./home";
              }}
            >
              Sign out
            </i>
          </li>
        </ul>
      </div>
    </div>
  );
};
