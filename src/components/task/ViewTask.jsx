import React, { useState, useEffect } from "react";
import { getUsers, deleteTask} from "../../services/taskService";
import { ListTasks } from "./ListTasks";
import Swal from "sweetalert2";
import { NewUserTaks } from "./NewUserTask";

export const ViewTask = () => {
  const [task, setTask] = useState([]);
  const [openModal, setOpenModal] = useState(false);


  const listarTaks = async () => {
    try {
      Swal.fire({
        allowOutsideClick: false,
        text: "Cargando...",
      });
      Swal.showLoading();
      const { data } = await getUsers();
      setTask(data);

      Swal.close();
    } catch (error) {
      console.log(error);
      Swal.close();
    }
  };

  useEffect(() => {
    listarTaks();
  }, []);


  const handleOpenModal = () => {
    setOpenModal(!openModal);
  };

  const deleteTasks = async (id) => {
    try {
      Swal.fire({
        allowOutsideClick: false,
        text: "Cargando...",
      });
      Swal.showLoading();
      await deleteTask(id);
      listarTaks();
      Swal.close();
    } catch (error) {
      console.log(error);
      Swal.close();
      let mensaje;
      if (error && error.response && error.response?.data) {
        mensaje = error.response?.data;
      } else {
        mensaje = "Ocurrio un error, por favor intente de nuevo";
      }
      Swal.fire("Error", mensaje, "error");
    }
  };

  return (
    <div className="container">
      <div className="row mt-3">
        <div className="col">
          <div className="sidebar-header">
            <h3>Tareas y usuarios</h3>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col">
          <hr />
        </div>
      </div>
      <div className="row">
        <div className="col-2">
          <div className="mb-3">
            <label className="form-label">Cedula</label>
            <input
              type="text"
              name="serial"
              required
              // value={serial}
              className="form-control"
            />
          </div>
        </div>
        
      </div>

      <hr />
      <ListTasks
        task={task}
        deleteTasks={deleteTasks}
      ></ListTasks>

      {openModal ? (
        <NewUserTaks
          handleOpenModal={handleOpenModal}
          listarTaks={listarTaks}
        />
      ) : (
        <button className="btn btn-primary fab" onClick={handleOpenModal}>
          <i className="fa-solid fa-plus"></i>
        </button>
       )} 

      <nav
        className={openModal ? "visually-hidden" : "page"}
        aria-label="Page navigation example"
      >
      </nav>
    </div>
  );
};
