import React from "react";
import { Link } from "react-router-dom";

export const ListTasks = (props) => {
  const {task, deleteTasks} = props;
  let array = task;
  return (
    <table className="table table-striped table-hover">
      <thead>
        <tr>
         <th scope="col">Cedula</th> 
          <th className="col">Nombre</th>
          <th scope="col">Tarea</th>
          <th scope="col">Acciones</th>


        </tr>
      </thead>
      <tbody>
        {array.map((user) => (
          <tr key={ user.id}>     
            <td>{user.id}</td>
            <td>{user.name} </td>
            <td>{ user.task} </td>
                <div >
                  <Link to={`/edit/${user.id}`}>
                    <button className=" mx-2 btn btn-warning">
                      <i className="fa-solid fa-pencil"></i>
                    </button>
                  </Link>
                  <button
                    className="btn btn-danger"
                    onClick={() => {
                      deleteTasks(user.id);
                    }}
                  >
                    <i className="fa-sharp fa-solid fa-trash"></i>
                  </button>
                </div>

          </tr>
        ))}
      </tbody>
    </table>
  );
};
