import React, { useState } from "react";
import { crearTask } from "../../services/taskService";
import Swal from "sweetalert2";

export const NewUserTaks= ({ handleOpenModal, listarTaks }) => {

  const [valoresForm, setValoresForm] = useState({});
  const {
    cedula = "",
    nombre = "",
    tarea = "",
   
  } = valoresForm;  
  
  const handleOnChange = ({ target }) => {
    const { name, value } = target;
    setValoresForm({ ...valoresForm, [name]: value });
  };

  const handleOnSubmit = async (e) => {
    e.preventDefault();
    const task = {
      cedula,
      nombre,
      tarea,     
    };

    try {
      Swal.fire({
        allowOutsideClick: false,
        text: "Cargando...",
      });
      Swal.showLoading();
      const { data } = await crearTask(task);
      console.log(data);
      Swal.close();
      handleOpenModal();
      listarTaks();
    } catch (error) {
      console.log(error);
      Swal.close();
      let mensaje;
      if (error && error.response && error.response.data) {
        mensaje = error.response.data;
      } else {
        mensaje = "Ocurrio un error, por favor intente de nuevo";
      }
      Swal.fire("Error", mensaje, "error");
    }
  };
  return (
    <div className=" view sidebar">
      <div className="container-fluid">
        <div className="row">
          <div className="col">
            <div className="sidebar-header">
              <h3>Nueva tarea </h3>
              <button className="not-border" onClick={handleOpenModal}>
                <i className="fa-solid fa-xmark"></i>
              </button>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col">
            <hr />
          </div>
        </div>
        <form onSubmit={(e) => handleOnSubmit(e)}>
          <div className="row">
            <div className="col">
              <div className="mb-3">
                <label className="form-label">Serial</label>
                <input
                  type="text"
                  name="cedula"
                  required
                  value={cedula}
                  onChange={(e) => handleOnChange(e)}
                  className="form-control"
                />
              </div>
            </div>
            <div className="col">
              <div className="mb-3">
                <label className="form-label">nombre</label>
                <input
                  type="text"
                  name="nombre"
                  required
                  value={nombre}
                  onChange={(e) => handleOnChange(e)}
                  className="form-control"
                />
              </div>
            </div>
            <div className="col">
              <div className="mb-3">
                <label className="form-label">tarea</label>
                <input
                  type="text"
                  name="tarea"
                  required
                  value={tarea}
                  onChange={(e) => handleOnChange(e)}
                  className="form-control"
                />
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col">
              <button className="btn btn-primary">Guardar</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
};
