import React, { useState, useEffect } from "react";
import { useParams, useNavigate } from "react-router-dom";
import {
  getTaskPorId,
  editTask
} from "../../services/taskService";

import Swal from "sweetalert2";

export const UpdateTask = () => {
  const history = useNavigate();
  const { taskId } = useParams();
  const [task, setTask] = useState({});
  const [valoresForm, setValoresForm] = useState({});
  
  const {
    cedula = "",
    nombre = "",
    tarea = "",
    
  } = valoresForm;

 
  const getTask = async () => {
    try {
      Swal.fire({
        allowOutsideClick: false,
        text: "Cargando...",
      });
      Swal.showLoading();
      const { data } = await getTaskPorId(taskId);
      setTask(data);
      Swal.close();
    } catch (error) {
      Swal.close();
    }
  };

  useEffect(() => {
    getTask();
  }, []);

  useEffect(() => {
    setValoresForm({
      cedula: task.id,
      nombre: task.name,
      tarea: task.task,
    
    });
  }, [task]);

  const handleOnChange = ({ target }) => {
    const { name, value } = target;
    setValoresForm({ ...valoresForm, [name]: value });
  };

  const handleOnSubmit = async (e) => {
    e.preventDefault();
    const task = {
      cedula,
      nombre,
      tarea,

    };

    try {
      Swal.fire({
        allowOutsideClick: false,
        text: "Cargando...",
      });
      
      await editTask(taskId, task);
      await Swal.fire(
        'Good job!',
        'You clicked the button!',
        'success'
      )
      history("/home");
      Swal.close();
    } catch (error) {
      console.log(error);
      console.log(error.response.data);
      Swal.close();
      let mensaje;
      if (error && error.response && error.response.data) {
        mensaje = error.response.data;
      } else {
        mensaje = "Ocurrio un error, por favor intente de nuevo";
      }
      Swal.fire("Error", mensaje, "error");
    }
  };

  return (
    <div className=" view container-fluid mt-3 mb-2">
      <div className="card">
        <div className="card-header">
          <h5 className="card-title">Detalle de la tarea</h5>
          <button
            className="not-border"
            onClick={() => {
              history("/home");
            }}
          >
            <i className="fa-solid fa-xmark"></i>
          </button>
        </div>
        <div className="card-body">
         
            <div className="col-md-8">
              <form onSubmit={(e) => handleOnSubmit(e)}>
               
                  <div className="col">
                    <div className="mb-3">
                      <label className="form-label">Cedula</label>
                      <input
                        type="text"
                        name="cedula"
                        required
                        disabled
                        value={cedula}
                        onChange={(e) => handleOnChange(e)}
                        className="form-control"
                      />
                    </div>
                  </div>
                  <div className="col">
                    <div className="mb-3">
                      <label className="form-label">Nombre</label>
                      <input
                        type="text"
                        name="nombre"
                        required
                        value={nombre}
                        onChange={(e) => handleOnChange(e)}
                        className="form-control"
                      />
                    </div>
                  </div>
                  <div className="col">
                    <div className="mb-3">
                      <label className="form-label">tarea</label>
                      <input
                        type="text"
                        name="tarea"
                        required
                        value={tarea}
                        onChange={(e) => handleOnChange(e)}
                        className="form-control"
                      />
                    </div>
                  </div>

                <div className="row">
                  <div className="col">
                    <button className="btn btn-primary">Guardar</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
  
  );
};
