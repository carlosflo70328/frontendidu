const deleteUser = async () => {
  const usuario = {
    nombre,
    email,
    rol,
    estado,
  };

  console.log(usuario);

  try {
    Swal.fire({
      allowOutsideClick: false,
      text: "Cargando...",
    });
    Swal.showLoading();
    await editUsuario(usuarioId, usuario);
    history.goBack();
    Swal.close();
  } catch (error) {
    console.log(error);
    console.log(error.response.data);
    Swal.close();
    let mensaje;
    if (error && error.response && error.response.data) {
      mensaje = error.response.data;
    } else {
      mensaje = "Ocurrio un error, por favor intente de nuevo";
    }
    Swal.fire("Error", mensaje, "error");
  }
};

export { borrarUsuario };
