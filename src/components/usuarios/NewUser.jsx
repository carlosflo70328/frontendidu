import React, { useState } from "react";
import { newUser } from "../../services/userService";
import Swal from "sweetalert2";
import md5 from "md5";

export const NewUser = ({ handleOpenModal, listarUsuarios }) => {
  const [valoresForm, setValoresForm] = useState({});
  const {
    nombre = "",
    email = "",
    rol = "",
    estado,
    password = "",
  } = valoresForm;

  const handleOnChange = ({ target }) => {
    const { name, value } = target;
    setValoresForm({ ...valoresForm, [name]: value });
  };

  const handleOnSubmit = async (e) => {
    e.preventDefault();
    const inventario = {
      nombre,
      email,
      rol,
      password: md5(password),
      estado,
    };

    try {
      Swal.fire({
        allowOutsideClick: false,
        text: "Cargando...",
      });
      Swal.showLoading();
      await newUser(inventario);
      Swal.close();
      handleOpenModal();
      listarUsuarios();
    } catch (error) {
      console.log(error);
      Swal.close();
      let mensaje;
      if (error && error.response && error.response.data) {
        mensaje = error.response.data;
      } else {
        mensaje = "Ocurrio un error, por favor intente de nuevo";
      }
      Swal.fire("Error", mensaje, "error");
    }
  };
  return (
    <div className=" view sidebar">
      <div className="container-fluid">
        <div className="row">
          <div className="col">
            <div className="sidebar-header">
              <h3>Nuevo Usuario</h3>
              <button className="not-border" onClick={handleOpenModal}>
                <i className="fa-solid fa-xmark"></i>
              </button>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col">
            <hr />
          </div>
        </div>
        <form onSubmit={(e) => handleOnSubmit(e)}>
          <div className="row">
            <div className="col">
              <div className="mb-3">
                <label className="form-label">Nombre</label>
                <input
                  type="text"
                  name="nombre"
                  required
                  value={nombre}
                  onChange={(e) => handleOnChange(e)}
                  className="form-control"
                />
              </div>
            </div>
            <div className="col">
              <div className="mb-3">
                <label className="form-label">Email</label>
                <input
                  type="text"
                  name="email"
                  required
                  value={email}
                  onChange={(e) => handleOnChange(e)}
                  className="form-control"
                />
              </div>
            </div>
            <div className="col">
              <div className="mb-3">
                <label className="form-label">Contraseña</label>
                <input
                  type="text"
                  name="password"
                  required
                  value={password}
                  onChange={(e) => handleOnChange(e)}
                  className="form-control"
                />
              </div>
            </div>
            <div className="col">
              <div className="mb-3">
                <label className="form-label">Rol</label>
                <select
                  className="form-select"
                  required
                  onChange={(e) => handleOnChange(e)}
                  name="rol"
                  value={rol}
                >
                  <option value="">SELECCIONE</option>
                  <option>Administrador</option>
                  <option>Tecnico</option>
                  <option>Auxiliar</option>
                </select>
              </div>
            </div>
            <div className="col">
              <div className="mb-3">
                <label className="form-label">Estado</label>
                <select
                  className="form-select"
                  required
                  onChange={(e) => handleOnChange(e)}
                  name="estado"
                  value={estado}
                >
                  <option value="">SELECCIONE</option>
                  <option>Activo</option>
                  <option>Inactivo</option>
                </select>
              </div>
            </div>
          </div>

          <div className="row">
            <div className="col">
              <button className="btn btn-primary">Guardar</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
};
