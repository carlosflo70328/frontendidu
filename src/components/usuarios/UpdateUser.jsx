import React, { useState, useEffect } from "react";
import { useParams, useNavigate } from "react-router-dom";
import { editUser, getUserId } from "../../services/userService";
import Swal from "sweetalert2";

export const UpdateUser = () => {
  const history = useNavigate();
  const { usuarioId } = useParams();
  const [usuario, setUsuario] = useState({});
  const [valoresForm, setValoresForm] = useState({});

  const { nombre = "", email = "", rol = "", estado } = valoresForm;

  const getUsuario = async () => {
    try {
      Swal.fire({
        allowOutsideClick: false,
        text: "Cargando...",
      });
      Swal.showLoading();
      const { data } = await getUserId(usuarioId);
      setUsuario(data);
      Swal.close();
    } catch (error) {
      console.log(error);
      Swal.close();
    }
  };

  useEffect(() => {
    getUsuario();
  }, [usuarioId]);

  useEffect(() => {
    setValoresForm({
      nombre: usuario.nombre,
      email: usuario.email,
      rol: usuario.rol,
      estado: usuario.estado,
    });
  }, [usuario]);

  const handleOnChange = ({ target }) => {
    const { name, value } = target;
    setValoresForm({ ...valoresForm, [name]: value });
  };

  const handleOnSubmit = async (e) => {
    e.preventDefault();
    const usuario = {
      nombre,
      email,
      rol,
      estado,
    };

    try {
      Swal.fire({
        allowOutsideClick: false,
        text: "Cargando...",
      });
      Swal.showLoading();
      await editUser(usuarioId, usuario);
      history.goBack();
      Swal.close();
    } catch (error) {
      console.log(error);
      console.log(error.response.data);
      Swal.close();
      let mensaje;
      if (error && error.response && error.response.data) {
        mensaje = error.response.data;
      } else {
        mensaje = "Ocurrio un error, por favor intente de nuevo";
      }
      Swal.fire("Error", mensaje, "error");
    }
  };

  return (
    <div className="view container-fluid mt-3 mb-2">
      <div className="card">
        <div className="card-header">
          <h5 className="card-title">Editar Usuario</h5>
          <button
            className="not-border"
            onClick={() => {
              history("/usuarios");
            }}
          >
            <i className="fa-solid fa-xmark"></i>
          </button>
        </div>
        <div className="card-body">
          <div className="row">
            <div className="col">
              <form onSubmit={(e) => handleOnSubmit(e)}>
                <div className="row">
                  <div className="col">
                    <div className="mb-3">
                      <label className="form-label">Nombre</label>
                      <input
                        type="text"
                        name="nombre"
                        required
                        value={nombre}
                        onChange={(e) => handleOnChange(e)}
                        className="form-control"
                      />
                    </div>
                  </div>
                  <div className="col">
                    <div className="mb-3">
                      <label className="form-label">Email</label>
                      <input
                        type="text"
                        name="email"
                        required
                        value={email}
                        onChange={(e) => handleOnChange(e)}
                        className="form-control"
                      />
                    </div>
                  </div>
                  <div className="col">
                    <div className="mb-3">
                      <label className="form-label">Rol</label>
                      <input
                        type="text"
                        name="rol"
                        required
                        value={rol}
                        onChange={(e) => handleOnChange(e)}
                        className="form-control"
                      />
                    </div>
                  </div>
                  <div className="col">
                    <div className="mb-3">
                      <label className="form-label">Estado</label>
                      <select
                        className="form-select"
                        required
                        onChange={(e) => handleOnChange(e)}
                        name="estado"
                        value={estado}
                      >
                        <option value="">SELECCIONE</option>
                        <option>Activo</option>
                        <option>Inactivo</option>
                      </select>
                    </div>
                  </div>
                </div>

                <div className="row">
                  <div className="col">
                    <button className="btn btn-primary">Guardar</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
