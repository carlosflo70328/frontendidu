import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { getUsers, deleteUSer } from "../../services/userService";
import { NewUser } from "./NewUser";
import Swal from "sweetalert2";

export const ViewUsers = () => {
  const [usuarios, setUsuarios] = useState([]);
  const [openModal, setOpenModal] = useState(false);

  const listarUsuarios = async () => {
    try {
      Swal.fire({
        allowOutsideClick: false,
        text: "Cargando...",
      });
      Swal.showLoading();
      const { data } = await getUsers();
      setUsuarios(data);
      Swal.close();
    } catch (error) {
      console.log(error);
      Swal.close();
    }
  };

  useEffect(() => {
    listarUsuarios();
  }, []);

  const handleOpenModal = () => {
    setOpenModal(!openModal);
  };

  const borrarUsuario = async (id) => {
    try {
      Swal.fire({
        allowOutsideClick: false,
        text: "Cargando...",
      });
      Swal.showLoading();
      await deleteUSer(id);
      listarUsuarios();
      Swal.close();
    } catch (error) {
      console.log(error);
      console.log(error.response.data);
      Swal.close();
      let mensaje;
      if (error && error.response && error.response.data) {
        mensaje = error.response.data;
      } else {
        mensaje = "Ocurrio un error, por favor intente de nuevo";
      }
      Swal.fire("Error", mensaje, "error");
    }
  };

  return (
    <div className=" container ">
      <table className="table table-striped table-hover">
        <thead>
          <tr>
            <th scope="col">Nombre</th>
            <th scope="col">Rol</th>
            <th scope="col">Correo</th>
            <th scope="col">Estado</th>
            <th scope="col">Fecha de creacion</th>
            <th scope="col">Acciones</th>
          </tr>
        </thead>
        <tbody>
          {usuarios.map((usuario, index) => (
            <tr key={usuario._id}>
              <td>{usuario.nombre}</td>
              <td>{usuario.rol}</td>
              <td>{usuario.email}</td>
              <td>{usuario.estado}</td>
              <td>{usuario.fechaCreacion}</td>
              <td>
                <div className="">
                  <Link to={`/usuarios/edit/${usuario._id}`}>
                    <button className=" mx-2 btn btn-warning">
                      <i className="fa-solid fa-pencil"></i>
                    </button>
                  </Link>
                  <button
                    className="btn btn-danger"
                    onClick={() => {
                      borrarUsuario(usuario._id);
                    }}
                  >
                    <i className="fa-sharp fa-solid fa-trash"></i>
                  </button>
                </div>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
      {openModal ? (
        <NewUser
          handleOpenModal={handleOpenModal}
          listarUsuarios={listarUsuarios}
        />
      ) : (
        <button className="btn btn-primary fab" onClick={handleOpenModal}>
          <i className="fa-solid fa-plus"></i>
        </button>
      )}
    </div>
  );
};
