import React, { useState, useEffect } from "react";
import { getMoves } from "../../services/inventarioService";
import { ListInventory } from "../inventarios/ListInventory";
import Swal from "sweetalert2";

export const ViewMoves = () => {
  const [inventarios, setInventarios] = useState([]);
  const [inventariosFil, setInventariosFil] = useState([]);
  const [currentPage, setCurrentPage] = useState(0);
  const [search, setSearch] = useState("");

  const listarInventarios = async () => {
    try {
      Swal.fire({
        allowOutsideClick: false,
        text: "Cargando...",
      });
      Swal.showLoading();
      const { data } = await getMoves();
      setInventarios(orderData(data));
      Swal.close();
    } catch (error) {
      console.log(error);
      Swal.close();
    }
  };

  useEffect(() => {
    listarInventarios();
  }, []);

  const orderData = (data) => {
    return data.sort(function (x, y) {
      const firstDate = new Date(y.fechaCreacion),
        SecondDate = new Date(x.fechaCreacion);

      if (firstDate < SecondDate) return -1;
      if (firstDate > SecondDate) return 1;
      return 0;
    });
  };

  // const filteredActivos = () => {
  //   if (search.length === 0) {
  //     setInventariosFil([]);
  //     return;
  //   }
  //   const arreglo1 = inventarios.filter((inventario) =>
  //     inventario.serial.includes(search)
  //   );
  //   setInventariosFil(arreglo1.slice(currentPage, currentPage + 10));
  //   return;
  // };

  // useEffect(() => {
  //   filteredActivos();
  // }, [search]);

  // const handleOnChange = ({ target }) => {
  //   setCurrentPage(0);
  //   setSearch(target.value);
  // };

  const nextPage = () => {
    if (
      inventarios.filter((inventario) => inventario.serial.includes(search))
        .length >
      currentPage + 10
    )
      setCurrentPage(currentPage + 2);
  };

  const prevPage = () => {
    if (currentPage > 0) setCurrentPage(currentPage - 10);
  };

  return (
    <div className="container">
      <div className="row mt-3">
        <div className="col">
          <div className="sidebar-header">
            <h3>Ultimos Movimientos</h3>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col">
          <hr />
        </div>
      </div>
      {/* <div className="row">
        <div className="col-2">
          <div className="mb-3">
            <label className="form-label">Serial</label>
            <input
              type="text"
              name="serial"
              required
              // value={serial}
              onChange={handleOnChange}
              className="form-control"
            />
          </div>
        </div>
      </div> */}
      <button className="btn btn-primary" onClick={prevPage}>
        Anteriores
      </button>
      &nbsp;
      <button className="btn btn-primary" onClick={nextPage}>
        Siguientes
      </button>
      <hr />
      <ListInventory
        inventarios={inventarios.slice(currentPage, currentPage + 10)}
        inventariosFil={inventariosFil.slice(currentPage, currentPage + 10)}
      ></ListInventory>
    </div>
  );
};
